FROM registry-gitlab.pasteur.fr/tru/docker-c7-miniconda-ci:latest
MAINTAINER Tru Huynh <tru@pasteur.fr>

RUN yum -y update && yum -y upgrade && \
	yum -y clean all

# update miniconda
RUN /opt/miniconda3/bin/conda update conda && /opt/miniconda3/bin/conda update --all
RUN /opt/miniconda3/bin/conda install tensorflow-gpu

# environment (should already be set from docker-c7-miniconda-ci)
#ENV PATH=/opt/miniconda3/bin:$PATH

RUN date +"%Y-%m-%d-%H%M" > /last_update
